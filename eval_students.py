import subprocess
import unittest
import subprocess
import os
import beautifultable
import numpy as np
import socket
import requests as r
import numpy as np

def warp_test_suite(testcase_class):
    """Load tests from a specific set of TestCase classes."""
    suite = unittest.TestSuite()
    tests = unittest.defaultTestLoader.loadTestsFromTestCase(testcase_class)
    suite.addTest(tests)
    return suite


with open("neptunids") as f:
    neptunids = []
    for line in f:
        port,nid = line.strip().split()
        neptunids.append((int(port),nid))


class AssignmentTestCase(unittest.TestCase):
    myport = 5007
    myneptun = "xjijm8"
    studentport = 0
    studentneptun = ""


    def setUp(self):
        self.a1 = "http://localhost:5007"
        self.a2 = "http://localhost:" + str(self.studentport)

    '''
    STUDENTS: START READING HERE

    Each of the functions below is a test your server needs to pass. 
    At each function you will see the input and I will explain the output.
    
    The assertEqual parts will compare your implementation's output to mine.
    '''
    def test_greet(self):
        '''
        Return the string "Hello, dear user!".
        '''
        url = "/"
        res1 = r.get(self.a1 + url)
        res2 = r.get(self.a2 + url)
        self.assertEqual(res1.text,res2.text)

    def test_echo(self):
        '''
        Echo back the string sent with the get method
        '''
        url = "/echo"
        payload = {"echostring": "habala echo this" }
        res1 = r.get(self.a1 + url,payload)
        res2 = r.get(self.a2 + url,payload)
        self.assertEqual(res1.text,res2.text)

    '''
    The following four functions will test whether your /calc route
    can accept the arguments a and b with GET and POST as well

    Both methods should return the same thing: I should be able to get a json
    with keys "sum" and "div" which are a+b and a/b-
    '''
    
    def test_getabsum(self):
        url = "/calc"
        payload = {"a": 2,"b":5 }
        res1 = r.get(self.a1 + url,payload)
        res2 = r.get(self.a2 + url,payload)
        self.assertEqual(res1.json()['sum'],res2.json()['sum'])

    def test_getabdiv(self):
        url = "/calc"
        payload = {"a": 4,"b":3 }
        res1 = r.get(self.a1 + url,payload)
        res2 = r.get(self.a2 + url,payload)
        self.assertEqual(res1.json()['div'],res2.json()['div'])

    def test_postabsum(self):
        url = "/calc"
        payload = {"a": 2,"b":5 }
        res1 = r.post(self.a1 + url,json=payload)
        res2 = r.post(self.a2 + url,json=payload)
        self.assertEqual(res1.json()['sum'],res2.json()['sum'])

    def test_postabdiv(self):
        url = "/calc"
        payload = {"a": 2,"b":5 }
        res1 = r.post(self.a1 + url,json=payload)
        res2 = r.post(self.a2 + url,json=payload)
        self.assertEqual(res1.json()['div'],res2.json()['div'])


    def test_control_fasta(self):
        '''
        This route should use the fasta module you wrote for the previous assignment

        We will send json data that will have three values:
            source: a url to fasta file
            function: this can be the following
                - num_with_motif -> returns the number of matching proteins by find_protein_with_motif 
                - num_amino_acids -> returns the total count of an aminoacid in all the proteins
            arg: the argument to the above function

        What we get back should be a json, with the following two keys:
            retval: should be the return value of the function sent or "bad function" if the function name was wrong
            num_proteins: the number of proteins in the fasta file
        '''
        url = "/fasta"
        payload = {
            "source": "https://bence.ferdinandy.com/content/images/files/pythonclass/proteins.fasta",
            "function": "num_with_motif",
            "arg": "AAA"
        }

        res1 = r.post(self.a1 + url,json = payload).json()
        res2 = r.post(self.a2 + url,json = payload).json()
        self.assertEqual([res1["retval"],res1['num_proteins']],[res2["retval"],res2['num_proteins']])

    def test_control_fasta_bad_func(self):
        '''
        '''
        url = "/fasta"
        payload = {
            "source": "https://bence.ferdinandy.com/content/images/files/pythonclass/proteins.fasta",
            "function": "bad_func",
            "arg": "AAA"
        }

        res1 = r.post(self.a1 + url,json = payload).json()
        res2 = r.post(self.a2 + url,json = payload).json()
        self.assertEqual(
                [res1["retval"],res1['num_proteins']],
                [res2["retval"],res2['num_proteins']]
        )


    def test_mongo_name(self):
        '''
        In the PYTHONCLASS2020 database's "main" collection find all the documents where iteration is the given number and return a list of the name variable of the documents
        '''
        url = "/mongo/names"
        payload = {
            "iteration": 2
        }

        res1 = r.post(self.a1 + url,json = payload).json()
        res2 = r.post(self.a2 + url,json = payload).json()
        self.assertEqual(
                sorted(res1),sorted(res2)
        )

    def test_mongo_clear(self):
        '''
        Delete all documents from PYTHONCLASS2020 database's [yourneptunid] collection.

        We need this for the evaluation, so your database is in a consistent state, but since the evaluation script runs every hour on the remote, implementing this will clear your databases every hour. Because of this, it might be a good idea to either leave it last or breaking it while working on the other stuff.

        It doesn't matter what the get returns, as long as it is a valid 200 status code.
        '''
        url = "/mongo/clear"

        res1 = r.get(self.a1 + url)
        res2 = r.get(self.a2 + url)
        
        self.assertEqual(
                res1.status_code,res2.status_code
        )

    def test_mongo_create_user(self):
        '''
        In your collection (so database PYTHONCLASS2020, collection [yourneptunid]), create a document which has the same keys as the payload. This is actually a very insecure way of handling passwords, but don't worry about that now :)
        Again, you should return a valid status code of 200, but doesnt matter what exactly.
        '''
        url = "/mongo/create_user"
        payload = {
            "username": "frocli99",
            "email": "frocli99@gmail.com",
            "password": "insecure_password",
            "name": "János Fröhlich"
        }

        res1 = r.post(self.a1 + url,json = payload)
        res2 = r.post(self.a2 + url,json = payload)
        
        self.assertEqual(
                res1.status_code,res2.status_code
        )


    def test_mongo_names_auth(self):
        '''
        This is similar to /mongo/names, except that the user also needs to provide a username and the corresponding password. Look up the user based on the username and if the user exists compare the passwords.
        
        It should return a json with

        {
            "list_of_names": [the list of names],
            "status": [status]
        }
        Status should be one of the three:
            - "ok"
            - "bad username"
            - "bad password"

        The list of names should only be provided if status is "ok".
        '''
        url = "/mongo/auth/names"
        payload = {
            "username": "frocli99",
            "password": "insecure_password",
            "iteration": 1
        }

        res1 = r.post(self.a1 + url,json = payload).json()
        res2 = r.post(self.a2 + url,json = payload).json()
        
        self.assertEqual(
                sorted(res1["list_of_names"]),
                sorted(res2["list_of_names"])
        )
    def test_mongo_names_auth_bad_user(self):
        '''
        '''
        url = "/mongo/auth/names"
        payload = {
            "username": "frocli999",
            "password": "insecure_password",
            "iteration": 1
        }

        res1 = r.post(self.a1 + url,json = payload).json()
        res2 = r.post(self.a2 + url,json = payload).json()
        
        self.assertEqual(
                sorted(res1["list_of_names"]),
                sorted(res2["list_of_names"])
        )
    def test_mongo_names_auth_bad_pwd(self):
        '''
        '''
        url = "/mongo/auth/names"
        payload = {
            "username": "frocli99",
            "password": "inssadfsdaf",
            "iteration": 1
        }

        res1 = r.post(self.a1 + url,json = payload).json()
        res2 = r.post(self.a2 + url,json = payload).json()
        
        self.assertEqual(
                res1["status"],
                res2["status"]
        )

    def test_svm(self):
        '''
        Using the iris dataset (https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_iris.html), an SVM model (svc with rbf kernel) return the probability and class (species) of the supplied data.

        Input is C and gamma, the two parameters of the rbf kernel, and data, which is a list of lists. The four numbers correspond to sepal width, length and petal width and length, and they should be in the same order as in the sklearn dataset (the order in this comment might be wrong).

        The return value should be a list of dictionaries. The order of the list should correspond to the order we sent the data and should have a "species" and a "probability" key e.g. with just one element
        [
            {
                "probability": 0.85,
                "species": "virginica"
            }
        ]


        '''
        url = "/svm"
        payload = {
            "C": 1,
            "gamma": 0.7,
            "data": [
                    [5,3,1,0.2],
                    [3,8,3,2]
                ]
        }

        res1 = r.post(self.a1 + url,json = payload).json()
        res2 = r.post(self.a2 + url,json = payload).json()
        self.assertEqual(
                [x["species"] for x in res1],
                [x["species"] for x in res2]
        )

    def test_svm2(self):
        '''
        '''
        url = "/svm"
        payload = {
            "C": 1,
            "gamma": 0.7,
            "data": [
                    [1,4,1,0.2],
                    [5,3,3,2]
                ]
        }

        res1 = r.post(self.a1 + url,json = payload).json()
        res2 = r.post(self.a2 + url,json = payload).json()
        p1 = np.array([x["probability"] for x in res1]) 
        p2 = np.array([x["probability"] for x in res2]) 

        self.assertAlmostEqual(np.sum(p1-p2),0,delta = 0.15)
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-p","--port",type=int, default = 5007)
    parser.add_argument("-n","--neptunid",type=str, default = "xjijm8")
    parser.add_argument("-e","--eval",action="store_true")
    args = parser.parse_args()

    runner = unittest.TextTestRunner()
    table = beautifultable.BeautifulTable() 
    results = {}
    table.columns.header = ["NEPTUN","SUBMISSION","SCORE","FEEDBACK"]
    if args.eval:
        for port,nid in neptunids:
            try:
                soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                soc.bind(("127.0.0.1",port))
                table.rows.append([nid,"no","-",""])
                soc.close()

            except socket.error as e:
                AssignmentTestCase.studentport = port
                AssignmentTestCase.studentneptun = nid
                res = runner.run(warp_test_suite(AssignmentTestCase))
                res.studentport  = port
                if len(res.errors) >= 1:
                    feedback = "\n".join(res.errors[-1][1].split("\n")[-3:-1])
                elif len(res.failures) >= 1:
                    feedback = res.failures[-1][1].split("\n")[-2]
                else:
                    feedback = "perfect"
                table.rows.append([nid,"yes",str(res.testsRun - (len(res.errors) + len(res.failures)))+ "/" + str(res.testsRun), feedback]) 
        table.rows.sort("NEPTUN")
        print(table)
    else:
        AssignmentTestCase.studentport = args.port
        AssignmentTestCase.studentneptun = args.neptunid
        res = runner.run(warp_test_suite(AssignmentTestCase))

