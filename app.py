from flask import Flask, jsonify, request
app = Flask(__name__)
import importlib
import pymongo

db = pymongo.MongoClient("localhost")["PYTHONCLASS2020"]

@app.route('/')
def hello():
	return "Hello, dear user!"

@app.route('/echo')
def echo():
	return request.args.get("echostring")

@app.route('/calc', methods=['GET', 'POST'])
def calc():
    a = 0
    b = 0
    if request.method == 'POST':
        jsondata = request.json
        a = int(jsondata['a'])
        b = int(jsondata['b'])
    else:
        a = int(request.args.get('a'))
        b = int(request.args.get('b'))
    result = json.dumps({'sum': a + b, 'div': a / b})
    return result

@app.route("/mongo/names", methods=["POST"])
def mongo():
	db = pymongo.MongoClient("localhost")["PYTHONCLASS2020"]["main"]
	namedata = []
	try:
		req=request.get_json()
		reqi = int(req["iteration"])
		for i in db.find({"iteration":reqi}):
			namedata.append(str(i["name"]))
		return jsonify(namedata)
	except TypeError:
		iter = int(request.args.get("iteration"))
		for i in db.find({"iteration":iter}):
			namedata.append(str(i["name"]))
		return jsonify(namedata)

@app.route("/mongo/clear", methods=["POST", "GET", "DELETE"])
def mongo_clear():
	mongo.db.hgkxos.delete_many({})
	return "clear"

